<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {   
    
    return view('welcome'); });



Auth::routes(); 
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cpanal', 'cpanal@index')->name('cpanal');
Route::get('post','Post@index');
Route::get('post/{id}','Post@update');

Route::get('coustomer','coustomer@mylist')->name('mylist');


Route::resource('posts','PostsController');
Route::resource('coustomer','CoustomerController');
Route::resource('password','PasswordController');
Route::resource('payment','PaymentController');