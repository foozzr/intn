<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use\App\coustomer;
use\DB;


class CoustomerController extends Controller
{  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       

        $users = coustomer::orderBy('id', 'DESC')->paginate(2);
        return view('coustomer.index')->with('users',$users);
        
        
    }

  


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('coustomer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required|max:255',
            'amount' => 'required|max:6',
            'component' => 'required|max:6',
            'phone' => 'required|max:10'
        ]);

        $newuser = new coustomer;
            $newuser->name  =  ucfirst($request->input('name'));
            $newuser->phone = $request->input('phone');
            $newuser->amount= $request->input('amount');
            $newuser->component_amount = $request->input('component');
           
           
            if($newuser->save()){
               return redirect("coustomer");
            }else{}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coustomer = coustomer::find($id);
        return view('coustomer.show')->with('user',$coustomer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $coustomer = coustomer::find($id);
        return view('coustomer.edit')->with('coustomer',$coustomer);

     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
               $this->validate($request,[
            'name' => 'required|max:255',
            'amount' => 'required|max:6',
            'component' => 'required|max:6',
            'phone' => 'required|max:10'
        ]);

        $edited_customer = coustomer::find($id);
            $edited_customer->name  =  ucfirst($request->input('name'));
            $edited_customer->phone = $request->input('phone');
            $edited_customer->amount= $request->input('amount');
            $edited_customer->component_amount = $request->input('component');
           
           
            if($edited_customer->save()){
               return redirect("coustomer");
            }else{}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
   
    return('I will delete..');
    }
}