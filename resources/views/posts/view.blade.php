@extends('layouts.app')
@section('content')

<div class="container card pt-4 mt-4 pb-4 mt-4">

        <a href="posts/create">New</a>

</div>



@if(count($posts)>0)
        @foreach ($posts as $post)
           <div class="card m-1 p-4">
                <a         href="posts/{{$post->id}}">   
           <strong><h5>  {{$post->title}}   </h5></strong>
      </a>
<h6>
        {{$post->body}}
</h6>
<small> Wretten on {{$post->created_at}}</small>
           </div>
           <br>
        @endforeach
@else
<hr> <h2>No Post found</h2>
@endif
@endsection


@section('js')
    <script type="text/javascript">
       //alert("Testng");
    </script>
@endsection