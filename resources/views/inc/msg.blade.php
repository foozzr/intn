<script type="text/javascript">
toastr.options = {
  "closeButton": true,
  "debug": true,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "md-toast-top-right",
  "preventDuplicates": true,
  "showDuration": 300,
  "hideDuration": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
    function errordisplay(newerror){
         toastr.error(newerror);
    }
    function successdisplay(newerror){
         toastr.success(newerror);
    }
    </script>
@if(count($errors) > 0)
    @foreach ($errors->all() as $error)
            <script type="text/javascript">
            errordisplay('{{$error}}');
            </script>
    @endforeach
@endif

@if(session('success'))
    <script type="text/javascript">
        successdisplay(' {{session('success')}}');
        </script>
@endif

@if(session('error'))
    <script type="text/javascript">
    errordisplay(' {{session('error')}}');
    </script>
@endif