@extends('layouts.app')
@section('content')

<style>
.dlt-btn{

  margin: 0px;
  padding: 1px 5px 0px 5px ;
  border-radius: 10px;
}
</style>

<div class="container">

        <nav class="navbar navbar-expand-lg navbar-light blue-grey lighten-5 ">

                <!-- Navbar brand -->
               <h5>
                   Coustomers
               </h5>
          
                <!-- Collapse button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          
                <!-- Collapsible content -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
          
                  <!-- Links -->
                  <ul class="navbar-nav mr-auto">
                   
          
                  </ul>
                  <!-- Links -->
          
                  <!-- Search form -->
                  <form class="form-inline">
                        <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  
                                    <button class="btn btn-primary btn-md m-0 px-3 py-2 z-depth-0 dropdown-toggle" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search With</button>
                                 
                                    <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Name</a>
                                    <a class="dropdown-item" href="#">Phone</a>
                                </div>

                              

                                <input type="text" class="form-control" aria-label="Text input with dropdown button">

                                


                              </div>
                        </div>
                            
                  </form>
                </div>
                <!-- Collapsible content -->
          
              </nav>



@if(count($users)>0)
                




        <table class="table table-hover table-dark">
                <thead>
                  <tr>
                   
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Component</th>
                    <th scope="col">Date of Issue</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>

                
                   
                  
                     
                     

                  @foreach ($users as $user)
                  <tr>
                      <th scope="row">{{$user->id}}</th>
                      <td>{{$user->name}}</td>
                      <td>{{$user->phone}}</td>
                      <td>{{$user->amount}}</td>
                      <td>{{$user->component_amount}}</td>
                      <td>{{$user->updated_at}}</td>
                      <td>
                      <a href="/coustomer/{{$user->id}}/edit" type="button" class="btn  btn-primary waves-effect waves-light dlt-btn">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                      </td>


                      <td>
                        

                         
                      <a href="/coustomer/{{$user->id}}" type="button" class="btn  btn-primary waves-effect waves-light dlt-btn">
                            Show
                        </a>
                         
                      
                        
                            <button  type="submit" name="dlt[]" value="{{$user->id}}" class="btn  btn-danger waves-effect waves-light dlt-btn">
                                D<i class="far fa-trash-alt"></i>
                            </button>

                        </td>




                    </tr>
                  @endforeach

               
                
                
                  @else
                  <hr class="card"> <h2>No user found...</h2>
                  @endif
               
              


                
                </tbody>
              </table>
             





              <nav aria-label="Page navigation example">
          <ul class="pagination pagination-circle pg-blue justify-content-center">
           
         
            {{$users->links()}}

          
          </ul>
        </nav>

</div>

@endsection
@section('js')
    <script type="text/javascript">
       //alert("Testng");
    </script>
@endsection