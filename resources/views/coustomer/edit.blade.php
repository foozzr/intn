@extends('layouts.app')
@section('content')



<div class="container col-12 col-md-auto">
        
        <div class="row justify-content-center mt-4 pt-4 mb-4 pb-4">
         


            <!-- Material form subscription -->
<div class="card">

       
    
        <!--Card content-->
        <div class="card-body px-lg-5">
    
            <!-- Form -->
         
                <form method="post" action="{{ route('coustomer.update',$coustomer->id) }}">
                      
                        @method('PUT')

                        
                <p>Enter the name or phone number of the customer.</p>
    
                <p>
                    <a href="" target="_blank">Find a customer</a>
                </p>
    
                <!-- Name -->
                <div class="md-form mt-3">
                <input type="text" name="name" id="name" class="form-control" value="{{$coustomer->name}}">
                    <label for="name">Name</label>
                </div>
    




                     
         
               
                        <div class="md-form">
                        <input type="number" name="amount"  id="number" class="form-control" value="{{$coustomer->amount}}">
                        <label for="amount">Amount</label>
                        </div>
                        <div class="md-form">
                        <input type="number" name="component"  id="component" class="form-control" value="{{$coustomer->component_amount}}">
                        <label for="component">Component</label>
                        </div>
                 



                
              
                <div class="md-form">
                    <input type="number" id="phone" name="phone" class="form-control" value="{{$coustomer->phone}}">
                    <label for="phone">Phone</label>
                </div>
    
                <!-- Sign in button -->
                <button class="btn btn-info  waves-effect" type="submit">Save</button>
                @csrf
            </form>
            <!-- Form -->
    
        </div>
    
    </div>
    <!-- Material form subscription -->
       
   </div>
 </div>


@endsection
@section('js')
    <script type="text/javascript">
       //alert("Testng");
    </script>
@endsection