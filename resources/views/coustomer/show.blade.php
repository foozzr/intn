@extends('layouts.app')
@section('content')

<style>
.dlt-btn{

  margin: 0px;
  padding: 1px 5px 0px 5px ;
  border-radius: 10px;
}
</style>

<div class="container-fluid">
<div class="container card pt-4 mt-4 pb-4 mt-4">

        




<table class="table table-hover table-dark">
                <thead>
                  <tr>
                   
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Component</th>
                    <th scope="col">Date of Issue</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>

                
                   
                  
                     
                     

               
                  <tr>
                      <th scope="row">{{$user->id}}</th>
                      <td>{{$user->name}}</td>
                      <td>{{$user->phone}}</td>
                      <td>{{$user->amount}}</td>
                      <td>{{$user->component_amount}}</td>
                      <td>{{$user->updated_at}}</td>
                      <td>
                    
                      </td>


                      <td>
                        

                         
                            
                         
                      
                        
                            <button  type="submit" name="dlt[]" value="{{$user->id}}" class="btn  btn-danger waves-effect waves-light dlt-btn">
                                Delete<i class="far fa-trash-alt"></i>
                            </button>

                        </td>




                    </tr>
             
          
              


                
                </tbody>
              </table>
             

             
</div>
</div>

@endsection
@section('js')
    <script type="text/javascript">
       //alert("Testng");
    </script>
@endsection